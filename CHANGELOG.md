# Changelog

# 2.1.0

- Added changelog
- Can access absolute and relative SASA for individual residues through `Result.residueAreas()`
- Can set options and classifier for a `Structure` initiated without an input file for later
  use in `Structure.addAtom()`
